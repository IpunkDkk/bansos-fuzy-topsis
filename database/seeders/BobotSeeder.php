<?php

namespace Database\Seeders;

use App\Models\Bobot;
use Illuminate\Database\Seeder;

class BobotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            ["C1", "Usia", "Benefit", 0.2],
            ["C2", "Pendapatan", "Cost", 0.15],
            ["C3", "Kebutuhan Sehari - hari", "Cost", 0.2],
            ["C4", "Kondisi Rumah", "Benefit", 0.25],
            ["C5", "Tanggungan", "Cost", 0.2],
        ];
        foreach ($data as $key => $value) {
            Bobot::create([
                'kode' => $value[0],
                'nilai_kriteria' => $value[1],
                'keterangan' => $value[2],
                'bobot' => $value[3],
            ]);
        }
    }
}
