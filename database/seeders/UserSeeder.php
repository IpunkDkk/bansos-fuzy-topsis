<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@email.com',
            'password' => bcrypt('password'),
        ]);
        $admin->assignRole('admin');
        $kepala = User::create([
            'name' => 'Kepala',
            'email' => 'kepala@email.com',
            'password' => bcrypt('password'),
        ]);
        $kepala->assignRole('kepala');
    }
}
