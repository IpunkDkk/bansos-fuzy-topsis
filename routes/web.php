<?php

use App\Http\Controllers\BobotController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DataController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
 */

Route::get('/', function () {
    // return view('welcome');
    return redirect()->route('login');
});

Route::get('/dashboard', [DashboardController::class, 'index'])->middleware(['auth'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'data'], function () {
    Route::get('/list', [DataController::class, 'index'])->name('data.index');
    Route::get('/detail/{id}', [DataController::class, 'show'])->name('data.show');
    Route::get('/create', [DataController::class, 'create'])->name('data.create')->middleware('role:admin');
    Route::post('/create', [DataController::class, 'store'])->name('data.store')->middleware('role:admin');
    Route::get('/edit/{id}', [DataController::class, 'edit'])->name('data.edit')->middleware('role:admin');
    Route::post('/edit/{id}', [DataController::class, 'update'])->name('data.update')->middleware('role:admin');

});

Route::group(['middleware' => ['auth'], 'prefix' => 'kriteria'], function () {
    Route::get('/list', [BobotController::class, 'index'])->name('kriteria.index');
    Route::get('/edit/{id}', [BobotController::class, 'edit'])->name('kriteria.edit')->middleware('role:admin');
    Route::post('/edit/{id}', [BobotController::class, 'update'])->name('kriteria.update')->middleware('role:admin');
});

require __DIR__ . '/auth.php';
