@extends('panels.master')


@push('css')
    <link href="{{ asset('') }}assets/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.11/dist/sweetalert2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css"
        integrity="sha512-O03ntXoVqaGUTAeAmvQ2YSzkCvclZEcPQu1eqloPaHfJ5RuNGiS4l+3duaidD801P50J28EHyonCV06CUlTSag=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush


@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Kriteria</h1>
        </div>
        <div class="card-body">
            <form action="{{ route('kriteria.update', $kriteria->id) }}" method="POST">
                @csrf
                <div class="mb-2">
                    <label for="kode">Kode</label>
                    <input required class="form-control" id="kode" name="kode" value="{{ $kriteria->kode }}">
                </div>
                <div class="mb-2">
                    <label for="nilai_kriteria">Nilai Kriteria</label>
                    <input required class="form-control" id="nilai_kriteria" name="nilai_kriteria"
                        value="{{ $kriteria->nilai_kriteria }}">
                </div>
                <div class="mb-2">
                    <label for="keterangan">Keterangan</label>
                    <input required class="form-control" id="keterangan" name="keterangan"
                        value="{{ $kriteria->keterangan }}">
                </div>
                <div class="mb-2">
                    <label for="bobot">Bobot</label>
                    <input required class="form-control" id="bobot" name="bobot" value="{{ $kriteria->bobot }}">
                </div>
                <button type="submit" class="btn btn-primary">Ubah</button>
            </form>
        </div>
    </div>
@endsection
