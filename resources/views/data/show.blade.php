@extends('panels.master')


@push('css')
    <link href="{{ asset('') }}assets/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.11/dist/sweetalert2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css"
        integrity="sha512-O03ntXoVqaGUTAeAmvQ2YSzkCvclZEcPQu1eqloPaHfJ5RuNGiS4l+3duaidD801P50J28EHyonCV06CUlTSag=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush


@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Details Data</h1>
        </div>
        <div class="card-body">
            <div class="card shadow mb-4">
                <a href="#rangking_data" class="d-block card-header py-3" data-toggle="collapse" role="button"
                    aria-expanded="true" aria-controls="rangking_data">
                    <h6 class="m-0 font-weight-bold text-primary">Rangking</h6>
                </a>
                <div class="collapse show" id="rangking_data">
                    <div class="card-body">
                        <div class="d-flex justify-content-center">
                            <h1 class="text-dark">{{ $rangking }}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card shadow mb-4">
                <a href="#data_input" class="d-block card-header py-3" data-toggle="collapse" role="button"
                    aria-expanded="true" aria-controls="data_input">
                    <h6 class="m-0 font-weight-bold text-primary">Data Input</h6>
                </a>
                <div class="collapse show" id="data_input">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <th>Alternative</th>
                                        <td>{{ $data->alternative }}</td>
                                        <th>usia</th>
                                        <td>{{ $data->usia }}</td>
                                    </tr>
                                    <tr>
                                        <th>pendapatan</th>
                                        <td>{{ $data->pendapatan }}</td>
                                        <th>kondisi_rumah</th>
                                        <td>{{ $data->kondisi_rumah }}</td>
                                    </tr>
                                    <tr>
                                        <th>Kebutuhan Sehari - hari</th>
                                        <td>{{ $data->kebutuhan_sehari_hari }}</td>
                                        <th>tanggungan</th>
                                        <td>{{ $data->tanggungan }}</td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card shadow mb-4">
                <a href="#usi_perhitungan" class="d-block card-header py-3" data-toggle="collapse" role="button"
                    aria-expanded="true" aria-controls="usi_perhitungan">
                    <h6 class="m-0 font-weight-bold text-primary">Usia</h6>
                </a>
                <div class="collapse show" id="usi_perhitungan">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr class="text-center">
                                        <th colspan="4">Usia</th>
                                    </tr>
                                    <tr class="text-center">
                                        <th>L</th>
                                        <th>M</th>
                                        <th>U</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="text-center">
                                        <td>{{ $data->usia_l }}</td>
                                        <td>{{ $data->usia_m }}</td>
                                        <td>{{ $data->usia_u }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card shadow mb-4">
                <a href="#pendapatan" class="d-block card-header py-3" data-toggle="collapse" role="button"
                    aria-expanded="true" aria-controls="pendapatan">
                    <h6 class="m-0 font-weight-bold text-primary">Pendapatan</h6>
                </a>
                <div class="collapse show" id="pendapatan">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr class="text-center">
                                        <th colspan="4">Pendapatan</th>
                                    </tr>
                                    <tr class="text-center">
                                        <th>L</th>
                                        <th>M</th>
                                        <th>U</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="text-center">
                                        <td>{{ $data->pendapatan_l }}</td>
                                        <td>{{ $data->pendapatan_m }}</td>
                                        <td>{{ $data->pendapatan_u }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card shadow mb-4">
                <a href="#kebutuhan_sehari_hari" class="d-block card-header py-3" data-toggle="collapse" role="button"
                    aria-expanded="true" aria-controls="kebutuhan_sehari_hari">
                    <h6 class="m-0 font-weight-bold text-primary">Kebutuhan Sehari-hari</h6>
                </a>
                <div class="collapse show" id="kebutuhan_sehari_hari">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr class="text-center">
                                        <th colspan="4">Kebutuhan Sehari-hari</th>
                                    </tr>
                                    <tr class="text-center">
                                        <th>L</th>
                                        <th>M</th>
                                        <th>U</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="text-center">
                                        <td>{{ $data->kebutuhan_l }}</td>
                                        <td>{{ $data->kebutuhan_m }}</td>
                                        <td>{{ $data->kebutuhan_u }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card shadow mb-4">
                <a href="#kondisi_rumah" class="d-block card-header py-3" data-toggle="collapse" role="button"
                    aria-expanded="true" aria-controls="kondisi_rumah">
                    <h6 class="m-0 font-weight-bold text-primary">Kondisi Rumah</h6>
                </a>
                <div class="collapse show" id="kondisi_rumah">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr class="text-center">
                                        <th colspan="4">Kondisi Rumah</th>
                                    </tr>
                                    <tr class="text-center">
                                        <th>L</th>
                                        <th>M</th>
                                        <th>U</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="text-center">
                                        <td>{{ $data->kondisi_rumah_l }}</td>
                                        <td>{{ $data->kondisi_rumah_m }}</td>
                                        <td>{{ $data->kondisi_rumah_u }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card shadow mb-4">
                <a href="#tanggungan" class="d-block card-header py-3" data-toggle="collapse" role="button"
                    aria-expanded="true" aria-controls="tanggungan">
                    <h6 class="m-0 font-weight-bold text-primary">Tanggungan</h6>
                </a>
                <div class="collapse show" id="tanggungan">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr class="text-center">
                                        <th colspan="4">Tanggungan</th>
                                    </tr>
                                    <tr class="text-center">
                                        <th>L</th>
                                        <th>M</th>
                                        <th>U</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="text-center">
                                        <td>{{ $data->tanggungan_l }}</td>
                                        <td>{{ $data->tanggungan_m }}</td>
                                        <td>{{ $data->tanggungan_u }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card shadow mb-4">
                <a href="#matrik" class="d-block card-header py-3" data-toggle="collapse" role="button"
                    aria-expanded="true" aria-controls="matrik">
                    <h6 class="m-0 font-weight-bold text-primary">Normalisasi Matrik</h6>
                </a>
                <div class="collapse show" id="matrik">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr class="text-center">
                                        <th colspan="5">Normalisasi Matrik</th>
                                    </tr>
                                    <tr class="text-center">
                                        <th>Usia</th>
                                        <th>Pendapatan</th>
                                        <th>Kebutuhan Sehari - hari</th>
                                        <th>Kondisi Rumah</th>
                                        <th>Tanggungan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="text-center">
                                        <td>{{ $data->matrik_usia }}</td>
                                        <td>{{ $data->matrik_pendapatan }}</td>
                                        <td>{{ $data->matrik_kebutuhan }}</td>
                                        <td>{{ $data->matrik_kondisi_rumah }}</td>
                                        <td>{{ $data->matrik_tanggungan }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card shadow mb-4">
                <a href="#pangkat_dua" class="d-block card-header py-3" data-toggle="collapse" role="button"
                    aria-expanded="true" aria-controls="pangkat_dua">
                    <h6 class="m-0 font-weight-bold text-primary">Pangkat Dua</h6>
                </a>
                <div class="collapse show" id="pangkat_dua">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr class="text-center">
                                        <th colspan="5">Pangkat Dua</th>
                                    </tr>
                                    <tr class="text-center">
                                        <th>Usia</th>
                                        <th>Pendapatan</th>
                                        <th>Kebutuhan Sehari - hari</th>
                                        <th>Kondisi Rumah</th>
                                        <th>Tanggungan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="text-center">
                                        <td>{{ $data->pangkat_usia }}</td>
                                        <td>{{ $data->pangkat_pendapatan }}</td>
                                        <td>{{ $data->pangkat_kebutuhan }}</td>
                                        <td>{{ $data->pangkat_kondisi_rumah }}</td>
                                        <td>{{ $data->pangkat_tanggungan }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card shadow mb-4">
                <a href="#keputusan_ternormalisasi" class="d-block card-header py-3" data-toggle="collapse"
                    role="button" aria-expanded="true" aria-controls="keputusan_ternormalisasi">
                    <h6 class="m-0 font-weight-bold text-primary">Keputusan Ternormalisasi</h6>
                </a>
                <div class="collapse show" id="keputusan_ternormalisasi">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr class="text-center">
                                        <th colspan="5">Keputusan Ternormalisasi</th>
                                    </tr>
                                    <tr class="text-center">
                                        <th>Usia</th>
                                        <th>Pendapatan</th>
                                        <th>Kebutuhan Sehari - hari</th>
                                        <th>Kondisi Rumah</th>
                                        <th>Tanggungan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="text-center">
                                        <td>{{ $data->keputusan_ternormalisasi_usia }}</td>
                                        <td>{{ $data->keputusan_ternormalisasi_pendapatan }}</td>
                                        <td>{{ $data->keputusan_ternormalisasi_kebutuhan }}</td>
                                        <td>{{ $data->keputusan_ternormalisasi_kondisi_rumah }}</td>
                                        <td>{{ $data->keputusan_ternormalisasi_tanggungan }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card shadow mb-4">
                <a href="#keputusan_ternormalisasi_bobot" class="d-block card-header py-3" data-toggle="collapse"
                    role="button" aria-expanded="true" aria-controls="keputusan_ternormalisasi_bobot">
                    <h6 class="m-0 font-weight-bold text-primary">Keputusan Ternormalisasi Terbobot</h6>
                </a>
                <div class="collapse show" id="keputusan_ternormalisasi_bobot">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr class="text-center">
                                        <th colspan="5">Keputusan Ternormalisasi Terbobot</th>
                                    </tr>
                                    <tr class="text-center">
                                        <th>Usia</th>
                                        <th>Pendapatan</th>
                                        <th>Kebutuhan Sehari - hari</th>
                                        <th>Kondisi Rumah</th>
                                        <th>Tanggungan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="text-center">
                                        <td>{{ $data->keputusan_ternormalisasi_usia_bobot }}</td>
                                        <td>{{ $data->keputusan_ternormalisasi_pendapatan_bobot }}</td>
                                        <td>{{ $data->keputusan_ternormalisasi_kebutuhan_bobot }}</td>
                                        <td>{{ $data->keputusan_ternormalisasi_kondisi_rumah_bobot }}</td>
                                        <td>{{ $data->keputusan_ternormalisasi_tanggungan_bobot }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
