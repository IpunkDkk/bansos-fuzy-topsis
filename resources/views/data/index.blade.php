@extends('panels.master')


@push('css')
    <link href="{{ asset('') }}assets/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.11/dist/sweetalert2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css"
        integrity="sha512-O03ntXoVqaGUTAeAmvQ2YSzkCvclZEcPQu1eqloPaHfJ5RuNGiS4l+3duaidD801P50J28EHyonCV06CUlTSag=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush


@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Data</h1>
            <form>
                <div class="d-flex justify-content-start">
                    <div class="mr-2">
                        <input placeholder="Banyak Layak" class="form-control" type="number" name="sorted_by"
                            value="{{ request('sorted_by') }}">
                    </div>
                    <button class="btn btn-warning">Set</button>
                </div>
            </form>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="data-older-table" class="table table-striped table-bordered dt-responsive nowrap"
                    style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Usia</th>
                            <th>Pendapatan</th>
                            <th>Kebutuhan Sehari - hari</th>
                            <th>Kondisi Rumah</th>
                            <th>Tanggungan</th>
                            <th>Rangking</th>
                            <th>Keterangan</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->alternative }}</td>
                                <td>{{ $item->usia }}</td>
                                <td>{{ $item->pendapatan }}</td>
                                <td>{{ $item->kebutuhan_sehari_hari }}</td>
                                <td>{{ $item->kondisi_rumah }}</td>
                                <td>{{ $item->tanggungan }}</td>
                                <td>{{ $item->ranking }}</td>
                                <td>{{ $item->keterangan }}</td>
                                <td>
                                    <div class="d-flex justify-content-center">
                                        @if (auth()->user()->getRoleNames()->first() == 'admin')
                                            <a class="btn btn-sm btn-warning"
                                                href="{{ route('data.edit', $item->id) }}">Edit</a>
                                        @endif
                                        <a class="btn btn-sm btn-info ml-2"
                                            href="{{ route('data.show', $item->id . '?rangking=' . $item->ranking) }}">Details</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection


@push('js')
    <script src="{{ asset('') }}assets/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('') }}assets/vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.11/dist/sweetalert2.all.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js"
        integrity="sha512-Zq9o+E00xhhR/7vJ49mxFNJ0KQw1E1TMWkPTxrWcnpfEFDEXgUiwJHIKit93EW/XxE31HSI5GEOW06G6BF1AtA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
        $(document).ready(function() {
            $('#data-older-table').DataTable({
                scrollX: true,
            });
        });
    </script>
@endpush
