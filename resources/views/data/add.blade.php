@extends('panels.master')


@push('css')
    <link href="{{ asset('') }}assets/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.11/dist/sweetalert2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css"
        integrity="sha512-O03ntXoVqaGUTAeAmvQ2YSzkCvclZEcPQu1eqloPaHfJ5RuNGiS4l+3duaidD801P50J28EHyonCV06CUlTSag=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush


@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Tambah Data</h1>
        </div>
        <div class="card-body">
            <form action="{{ route('data.store') }}" method="POST">
                @csrf
                <div class="mb-2">
                    <label for="nama">Nama</label>
                    <input required type="text" class="form-control" id="nama" name="nama">
                </div>
                <div class="mb-2">
                    <label for="usia">Usia</label>
                    <input required type="number" class="form-control" id="usia" name="usia">
                </div>
                <div class="mb-2">
                    <label for="pendapatan">Pendapatan</label>
                    <input class="form-control" id="pendapatan" name="pendapatan">
                </div>
                <div class="mb-2">
                    <label for="kebutuhan_sehari_hari">Kebutuhan Sehari-hari</label>
                    <input name="kebutuhan_sehari_hari" id="kebutuhan_sehari_hari" class="form-control">
                </div>
                <div class="mb-2">
                    <label for="kondisi_rumah">Kondisi Rumah</label>
                    <input name="kondisi_rumah" id="kondisi_rumah" class="form-control">

                </div>
                <div class="mb-2">
                    <label for="tanggungan">Tanggungan</label>
                    <input name="tanggungan" id="tanggungan" class="form-control">

                </div>
                <button type="submit" class="btn btn-primary">Tambah</button>
            </form>
        </div>
    </div>
@endsection
