<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-warning sidebar sidebar-warning accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('dashboard') }}">
        <div class="sidebar-brand-text mx-3">{{ config('app.name', 'Laravel') }}<sup>V0</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{ request()->segment(1) == 'dashboard' ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('dashboard') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>



    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Data
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item {{ request()->segment(1) == 'data' ? 'active' : '' }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#data" aria-expanded="true"
            aria-controls="data">
            <i class="fas fa-fw fa-database"></i>
            <span>Data</span>
        </a>
        <div id="data" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Menu</h6>
                @if (auth()->user()->getRoleNames()->first() == 'admin')
                    <a class="collapse-item {{ request()->segment(2) == 'create' && request()->segment(1) == 'data' ? 'active' : '' }}"
                        href="{{ route('data.create') }}">Tambah Data</a>
                @endif
                <a class="collapse-item {{ request()->segment(2) == 'list' && request()->segment(1) == 'data' ? 'active' : '' }}"
                    href="{{ route('data.index') }}">List Data</a>
            </div>
        </div>
    </li>

    <li class="nav-item {{ request()->segment(1) == 'kriteria' ? 'active' : '' }}">
        <a class="nav-link collapsed" href="{{ route('kriteria.index') }}" aria-expanded="true"
            aria-controls="kriteria">
            <i class="fas fa-fw fa-cog"></i>
            <span>Kriteria</span>
        </a>
    </li>






</ul>
<!-- End of Sidebar -->
