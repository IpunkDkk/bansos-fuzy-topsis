<?php

namespace App\Http\Controllers;

use App\Models\Bobot;
use App\Models\Data;
use Illuminate\Http\Request;

class DataController extends Controller
{
    public function index(Request $request)
    {
        $bobot = Bobot::all();
        $data = Data::all();
        $data = $this->fuzzy_topsis($data, $request, $bobot);
        return view('data.index', compact('data'));
    }
    public function create()
    {
        return view('data.add');
    }
    public function store(Request $request)
    {

        if ($request->usia < 20) {
            $usia = 4;
        } elseif ($request->usia < 40) {
            $usia = 3;
        } elseif ($request->usia >= 40 && $request->usia <= 55) {
            $usia = 2;
        } else {
            $usia = 1;
        }

        $data = Data::create([
            'alternative' => $request->nama,
            'usia' => $usia,
            'pendapatan' => $request->pendapatan,
            'kebutuhan_sehari_hari' => $request->kebutuhan_sehari_hari,
            'kondisi_rumah' => $request->kondisi_rumah,
            'tanggungan' => $request->tanggungan,
        ]);
        if (!$data) {
            return redirect()->back()->with('error', 'Data gagal ditambahkan');
        }
        return redirect()->route('data.index')->with('success', 'Data berhasil ditambahkan');
    }
    public function edit($id)
    {
        $data = Data::findOrFail($id);
        return view('data.edit', compact('data'));
    }
    public function update(Request $request, $id)
    {
        $data = Data::findOrFail($id);
        $data->update([
            'alternative' => $request->alternative,
            'usia' => $request->usia,
            'pendapatan' => $request->pendapatan,
            'kebutuhan_sehari_hari' => $request->kebutuhan_sehari_hari,
            'kondisi_rumah' => $request->kondisi_rumah,
            'tanggungan' => $request->tanggungan,
        ]);
        if (!$data) {
            return redirect()->back()->with('error', 'Data gagal diubah');
        }
        return redirect()->route('data.index')->with('success', 'Data berhasil diubah');
    }
    public function show($id, Request $request)
    {
        $rangking = $request->rangking;
        $data = Data::findOrFail($id);
        $bobot = Bobot::all();
        $data = $this->singgle_fuzzy_topsis($data, $bobot);
        // dd($data);
        return view('data.show', compact('data', 'rangking'));
    }
}
