<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;
    public function fuzzy_topsis($data, $request, $bobot)
    {
        foreach ($data as $item) {
            // usia l
            // usia l
            if ($item->usia == 1) {
                $item['usia_l'] = 0;
            } elseif ($item->usia == 2) {
                $item['usia_l'] = 0;
            } elseif ($item->usia == 3) {
                $item['usia_l'] = 0.25;
            } elseif ($item->usia == 4) {
                $item['usia_l'] = 0.5;
            } elseif ($item->usia == 5) {
                $item['usia_l'] = 0.75;
            }
            // usia m
            if ($item->usia == 1) {
                $item['usia_m'] = 0;
            } elseif ($item->usia == 2) {
                $item['usia_m'] = 0.25;
            } elseif ($item->usia == 3) {
                $item['usia_m'] = 0.5;
            } elseif ($item->usia == 4) {
                $item['usia_m'] = 0.75;
            } elseif ($item->usia == 5) {
                $item['usia_m'] = 1;
            }

            // usia u
            if ($item->usia == 1) {
                $item['usia_u'] = 0.25;
            } elseif ($item->usia == 2) {
                $item['usia_u'] = 0.5;
            } elseif ($item->usia == 3) {
                $item['usia_u'] = 0.75;
            } elseif ($item->usia == 4) {
                $item['usia_u'] = 1;
            } elseif ($item->usia == 5) {
                $item['usia_u'] = 1;
            }
            // pendapatan l
            if ($item->pendapatan == 1) {
                $item['pendapatan_l'] = 0;
            } elseif ($item->pendapatan == 2) {
                $item['pendapatan_l'] = 0;
            } elseif ($item->pendapatan == 3) {
                $item['pendapatan_l'] = 0.25;
            } elseif ($item->pendapatan == 4) {
                $item['pendapatan_l'] = 0.5;
            } elseif ($item->pendapatan == 5) {
                $item['pendapatan_l'] = 0.75;
            }

            // pendapatan m
            if ($item->pendapatan == 1) {
                $item['pendapatan_m'] = 0;
            } elseif ($item->pendapatan == 2) {
                $item['pendapatan_m'] = 0.25;
            } elseif ($item->pendapatan == 3) {
                $item['pendapatan_m'] = 0.5;
            } elseif ($item->pendapatan == 4) {
                $item['pendapatan_m'] = 0.75;
            } elseif ($item->pendapatan == 5) {
                $item['pendapatan_m'] = 1;
            }

            // pendapatan u
            if ($item->pendapatan == 1) {
                $item['pendapatan_u'] = 0.25;
            } elseif ($item->pendapatan == 2) {
                $item['pendapatan_u'] = 0.5;
            } elseif ($item->pendapatan == 3) {
                $item['pendapatan_u'] = 0.75;
            } elseif ($item->pendapatan == 4) {
                $item['pendapatan_u'] = 1;
            } elseif ($item->pendapatan == 5) {
                $item['pendapatan_u'] = 1;
            }

            // kebutahan l
            if ($item->kebutuhan_sehari_hari == 1) {
                $item['kebutuhan_l'] = 0;
            } elseif ($item->kebutuhan_sehari_hari == 2) {
                $item['kebutuhan_l'] = 0;
            } elseif ($item->kebutuhan_sehari_hari == 3) {
                $item['kebutuhan_l'] = 0.25;
            } elseif ($item->kebutuhan_sehari_hari == 4) {
                $item['kebutuhan_l'] = 0.5;
            } elseif ($item->kebutuhan_sehari_hari == 5) {
                $item['kebutuhan_l'] = 0.75;
            }

            // kebutuhan m
            if ($item->kebutuhan_sehari_hari == 1) {
                $item['kebutuhan_m'] = 0;
            } elseif ($item->kebutuhan_sehari_hari == 2) {
                $item['kebutuhan_m'] = 0.25;
            } elseif ($item->kebutuhan_sehari_hari == 3) {
                $item['kebutuhan_m'] = 0.5;
            } elseif ($item->kebutuhan_sehari_hari == 4) {
                $item['kebutuhan_m'] = 0.75;
            } elseif ($item->kebutuhan_sehari_hari == 5) {
                $item['kebutuhan_m'] = 1;
            }

            // kebutuhan u
            if ($item->kebutuhan_sehari_hari == 1) {
                $item['kebutuhan_u'] = 0.25;
            } elseif ($item->kebutuhan_sehari_hari == 2) {
                $item['kebutuhan_u'] = 0.5;
            } elseif ($item->kebutuhan_sehari_hari == 3) {
                $item['kebutuhan_u'] = 0.75;
            } elseif ($item->kebutuhan_sehari_hari == 4) {
                $item['kebutuhan_u'] = 1;
            } elseif ($item->kebutuhan_sehari_hari == 5) {
                $item['kebutuhan_u'] = 1;
            }

            // kondisi_rumah l
            if ($item->kondisi_rumah == 1) {
                $item['kondisi_rumah_l'] = 0;
            } elseif ($item->kondisi_rumah == 2) {
                $item['kondisi_rumah_l'] = 0;
            } elseif ($item->kondisi_rumah == 3) {
                $item['kondisi_rumah_l'] = 0.25;
            } elseif ($item->kondisi_rumah == 4) {
                $item['kondisi_rumah_l'] = 0.5;
            } elseif ($item->kondisi_rumah == 5) {
                $item['kondisi_rumah_l'] = 0.75;
            }
            // kondisi_rumah m
            if ($item->kondisi_rumah == 1) {
                $item['kondisi_rumah_m'] = 0;
            } elseif ($item->kondisi_rumah == 2) {
                $item['kondisi_rumah_m'] = 0.25;
            } elseif ($item->kondisi_rumah == 3) {
                $item['kondisi_rumah_m'] = 0.5;
            } elseif ($item->kondisi_rumah == 4) {
                $item['kondisi_rumah_m'] = 0.75;
            } elseif ($item->kondisi_rumah == 5) {
                $item['kondisi_rumah_m'] = 1;
            }

            // kondisi_rumah u
            if ($item->kondisi_rumah == 1) {
                $item['kondisi_rumah_u'] = 0.25;
            } elseif ($item->kondisi_rumah == 2) {
                $item['kondisi_rumah_u'] = 0.5;
            } elseif ($item->kondisi_rumah == 3) {
                $item['kondisi_rumah_u'] = 0.75;
            } elseif ($item->kondisi_rumah == 4) {
                $item['kondisi_rumah_u'] = 1;
            } elseif ($item->kondisi_rumah == 5) {
                $item['kondisi_rumah_u'] = 1;
            }

            // tanggungan l
            if ($item->tanggungan == 1) {
                $item['kondisi_rumah_l'] = 0;
            } elseif ($item->tanggungan == 2) {
                $item['kondisi_rumah_l'] = 0;
            } elseif ($item->tanggungan == 3) {
                $item['kondisi_rumah_l'] = 0.25;
            } elseif ($item->tanggungan == 4) {
                $item['kondisi_rumah_l'] = 0.5;
            } elseif ($item->tanggungan == 5) {
                $item['kondisi_rumah_l'] = 0.75;
            }
            // tanggungan m
            if ($item->tanggungan == 1) {
                $item['tanggungan_m'] = 0;
            } elseif ($item->tanggungan == 2) {
                $item['tanggungan_m'] = 0.25;
            } elseif ($item->tanggungan == 3) {
                $item['tanggungan_m'] = 0.5;
            } elseif ($item->tanggungan == 4) {
                $item['tanggungan_m'] = 0.75;
            } elseif ($item->tanggungan == 5) {
                $item['tanggungan_m'] = 1;
            }
            // tanggungan u
            if ($item->tanggungan == 1) {
                $item['tanggungan_u'] = 0.25;
            } elseif ($item->tanggungan == 2) {
                $item['tanggungan_u'] = 0.5;
            } elseif ($item->tanggungan == 3) {
                $item['tanggungan_u'] = 0.75;
            } elseif ($item->tanggungan == 4) {
                $item['tanggungan_u'] = 1;
            } elseif ($item->tanggungan == 5) {
                $item['tanggungan_u'] = 1;
            }

            // matrik usia
            $item['matrik_usia'] = ($item['usia_l'] + $item['usia_m'] + $item['usia_u']) / 3;
            // matrik pendapatan
            $item['matrik_pendapatan'] = ($item['pendapatan_l'] + $item['pendapatan_m'] + $item['pendapatan_u']) / 3;
            // matrik kebutuhan
            $item['matrik_kebutuhan'] = ($item['kebutuhan_l'] + $item['kebutuhan_m'] + $item['kebutuhan_u']) / 3;
            // matrik kondisi_rumah
            $item['matrik_kondisi_rumah'] = ($item['kondisi_rumah_l'] + $item['kondisi_rumah_m'] + $item['kondisi_rumah_u']) / 3;
            // matrik tanggungan
            $item['matrik_tanggungan'] = ($item['tanggungan_l'] + $item['tanggungan_m'] + $item['tanggungan_u']) / 3;
            // pangkat usia
            $item['pangkat_usia'] = pow($item['matrik_usia'], 2);
            // pangkat pendapatan
            $item['pangkat_pendapatan'] = pow($item['matrik_pendapatan'], 2);
            // pangkat kebutuhan
            $item['pangkat_kebutuhan'] = pow($item['matrik_kebutuhan'], 2);
            // pangkat kondisi_rumah
            $item['pangkat_kondisi_rumah'] = pow($item['matrik_kondisi_rumah'], 2);
            // pangkat tanggungan
            $item['pangkat_tanggungan'] = pow($item['matrik_tanggungan'], 2);
            // keputusan ternormalisasi usia
            $item['keputusan_ternormalisasi_usia'] = $item['matrik_usia'] / sqrt(array_sum([$item['pangkat_usia'], $item['pangkat_pendapatan'], $item['pangkat_kebutuhan'], $item['pangkat_kondisi_rumah'], $item['pangkat_tanggungan']]));
            // keputusan ternormalisasi pendapatan
            $item['keputusan_ternormalisasi_pendapatan'] = $item['matrik_pendapatan'] / sqrt(array_sum([$item['pangkat_usia'], $item['pangkat_pendapatan'], $item['pangkat_kebutuhan'], $item['pangkat_kondisi_rumah'], $item['pangkat_tanggungan']]));
            // keputusan ternormalisasi kebutuhan
            $item['keputusan_ternormalisasi_kebutuhan'] = $item['matrik_kebutuhan'] / sqrt(array_sum([$item['pangkat_usia'], $item['pangkat_pendapatan'], $item['pangkat_kebutuhan'], $item['pangkat_kondisi_rumah'], $item['pangkat_tanggungan']]));
            // keputusan ternormalisasi kondisi_rumah
            $item['keputusan_ternormalisasi_kondisi_rumah'] = $item['matrik_kondisi_rumah'] / sqrt(array_sum([$item['pangkat_usia'], $item['pangkat_pendapatan'], $item['pangkat_kebutuhan'], $item['pangkat_kondisi_rumah'], $item['pangkat_tanggungan']]));
            // keputusan ternormalisasi tanggungan
            $item['keputusan_ternormalisasi_tanggungan'] = $item['matrik_tanggungan'] / sqrt(array_sum([$item['pangkat_usia'], $item['pangkat_pendapatan'], $item['pangkat_kebutuhan'], $item['pangkat_kondisi_rumah'], $item['pangkat_tanggungan']]));
            // keputusan ternormalisasi usia bobot
            $item['keputusan_ternormalisasi_usia_bobot'] = $item['keputusan_ternormalisasi_usia'] * $bobot['0']->bobot;
            // keputusan ternormalisasi pendapatan bobot
            $item['keputusan_ternormalisasi_pendapatan_bobot'] = $item['keputusan_ternormalisasi_pendapatan'] * $bobot['1']->bobot;
            // keputusan ternormalisasi kebutuhan bobot
            $item['keputusan_ternormalisasi_kebutuhan_bobot'] = $item['keputusan_ternormalisasi_kebutuhan'] * $bobot['2']->bobot;
            // keputusan ternormalisasi kondisi_rumah bobot
            $item['keputusan_ternormalisasi_kondisi_rumah_bobot'] = $item['keputusan_ternormalisasi_kondisi_rumah'] * $bobot['3']->bobot;
            // keputusan ternormalisasi tanggungan bobot
            $item['keputusan_ternormalisasi_tanggungan_bobot'] = $item['keputusan_ternormalisasi_tanggungan'] * $bobot['4']->bobot;
            // dd($item);
        }
        //positif usia
        $positif['usia'] = $data->min('keputusan_ternormalisasi_usia_bobot');
        //positif pendapatan
        $positif['pendapatan'] = $data->min('keputusan_ternormalisasi_pendapatan_bobot');
        //positif kebutuhan
        $positif['kebutuhan'] = $data->min('keputusan_ternormalisasi_kebutuhan_bobot');
        //positif kondisi_rumah
        $positif['kondisi_rumah'] = $data->min('keputusan_ternormalisasi_kondisi_rumah_bobot');
        //positif tanggungan
        $positif['tanggungan'] = $data->min('keputusan_ternormalisasi_tanggungan_bobot');
        //negatif usia
        $negatif['usia'] = $data->max('keputusan_ternormalisasi_usia_bobot');
        //negatif pendapatan
        $negatif['pendapatan'] = $data->max('keputusan_ternormalisasi_pendapatan_bobot');
        //negatif kebutuhan
        $negatif['kebutuhan'] = $data->max('keputusan_ternormalisasi_kebutuhan_bobot');
        //negatif kondisi_rumah
        $negatif['kondisi_rumah'] = $data->max('keputusan_ternormalisasi_kondisi_rumah_bobot');
        //negatif tanggungan
        $negatif['tanggungan'] = $data->max('keputusan_ternormalisasi_tanggungan_bobot');

        foreach ($data as $item) {
            // d+
            $item['d_plus'] = sqrt(array_sum([
                pow($item['keputusan_ternormalisasi_usia_bobot'] - $positif['usia'], 2),
                pow($item['keputusan_ternormalisasi_pendapatan_bobot'] - $positif['pendapatan'], 2),
                pow($item['keputusan_ternormalisasi_kebutuhan_bobot'] - $positif['kebutuhan'], 2),
                pow($item['keputusan_ternormalisasi_kondisi_rumah_bobot'] - $positif['kondisi_rumah'], 2),
                pow($item['keputusan_ternormalisasi_tanggungan_bobot'] - $positif['tanggungan'], 2),
            ]));
            // d-
            $item['d_min'] = sqrt(array_sum([
                pow($item['keputusan_ternormalisasi_usia_bobot'] - $negatif['usia'], 2),
                pow($item['keputusan_ternormalisasi_pendapatan_bobot'] - $negatif['pendapatan'], 2),
                pow($item['keputusan_ternormalisasi_kebutuhan_bobot'] - $negatif['kebutuhan'], 2),
                pow($item['keputusan_ternormalisasi_kondisi_rumah_bobot'] - $negatif['kondisi_rumah'], 2),
                pow($item['keputusan_ternormalisasi_tanggungan_bobot'] - $negatif['tanggungan'], 2),
            ]));
            // preferensi
            $item['preferensi'] = $item['d_min'] / ($item['d_min'] + $item['d_plus']);
        }
        //ranking
        $ranking = 1;
        foreach ($data->sortByDesc('preferensi') as $key => $item) {
            $item['ranking'] = $ranking++;
            if ($request->sorted_by) {
                if ($item['ranking'] <= $request->sorted_by) {
                    $item['keterangan'] = 'Layak';
                } else {
                    $item['keterangan'] = 'Tidak Layak';
                }
            } else {
                if ($item['ranking'] <= 100) {
                    $item['keterangan'] = 'Layak';
                } else {
                    $item['keterangan'] = 'Tidak Layak';
                }
            }
        }
        return $data;
    }
    public function singgle_fuzzy_topsis($data, $bobot)
    {
        // usia l
        if ($data->usia == 1) {
            $data['usia_l'] = 0;
        } elseif ($data->usia == 2) {
            $data['usia_l'] = 0;
        } elseif ($data->usia == 3) {
            $data['usia_l'] = 0.25;
        } elseif ($data->usia == 4) {
            $data['usia_l'] = 0.5;
        } elseif ($data->usia == 5) {
            $data['usia_l'] = 0.75;
        }
        // usia m
        if ($data->usia == 1) {
            $data['usia_m'] = 0;
        } elseif ($data->usia == 2) {
            $data['usia_m'] = 0.25;
        } elseif ($data->usia == 3) {
            $data['usia_m'] = 0.5;
        } elseif ($data->usia == 4) {
            $data['usia_m'] = 0.75;
        } elseif ($data->usia == 5) {
            $data['usia_m'] = 1;
        }

        // usia u
        if ($data->usia == 1) {
            $data['usia_u'] = 0.25;
        } elseif ($data->usia == 2) {
            $data['usia_u'] = 0.5;
        } elseif ($data->usia == 3) {
            $data['usia_u'] = 0.75;
        } elseif ($data->usia == 4) {
            $data['usia_u'] = 1;
        } elseif ($data->usia == 5) {
            $data['usia_u'] = 1;
        }
        // pendapatan l
        if ($data->pendapatan == 1) {
            $data['pendapatan_l'] = 0;
        } elseif ($data->pendapatan == 2) {
            $data['pendapatan_l'] = 0;
        } elseif ($data->pendapatan == 3) {
            $data['pendapatan_l'] = 0.25;
        } elseif ($data->pendapatan == 4) {
            $data['pendapatan_l'] = 0.5;
        } elseif ($data->pendapatan == 5) {
            $data['pendapatan_l'] = 0.75;
        }

        // pendapatan m
        if ($data->pendapatan == 1) {
            $data['pendapatan_m'] = 0;
        } elseif ($data->pendapatan == 2) {
            $data['pendapatan_m'] = 0.25;
        } elseif ($data->pendapatan == 3) {
            $data['pendapatan_m'] = 0.5;
        } elseif ($data->pendapatan == 4) {
            $data['pendapatan_m'] = 0.75;
        } elseif ($data->pendapatan == 5) {
            $data['pendapatan_m'] = 1;
        }

        // pendapatan u
        if ($data->pendapatan == 1) {
            $data['pendapatan_u'] = 0.25;
        } elseif ($data->pendapatan == 2) {
            $data['pendapatan_u'] = 0.5;
        } elseif ($data->pendapatan == 3) {
            $data['pendapatan_u'] = 0.75;
        } elseif ($data->pendapatan == 4) {
            $data['pendapatan_u'] = 1;
        } elseif ($data->pendapatan == 5) {
            $data['pendapatan_u'] = 1;
        }

        // kebutahan l
        if ($data->kebutuhan_sehari_hari == 1) {
            $data['kebutuhan_l'] = 0;
        } elseif ($data->kebutuhan_sehari_hari == 2) {
            $data['kebutuhan_l'] = 0;
        } elseif ($data->kebutuhan_sehari_hari == 3) {
            $data['kebutuhan_l'] = 0.25;
        } elseif ($data->kebutuhan_sehari_hari == 4) {
            $data['kebutuhan_l'] = 0.5;
        } elseif ($data->kebutuhan_sehari_hari == 5) {
            $data['kebutuhan_l'] = 0.75;
        }

        // kebutuhan m
        if ($data->kebutuhan_sehari_hari == 1) {
            $data['kebutuhan_m'] = 0;
        } elseif ($data->kebutuhan_sehari_hari == 2) {
            $data['kebutuhan_m'] = 0.25;
        } elseif ($data->kebutuhan_sehari_hari == 3) {
            $data['kebutuhan_m'] = 0.5;
        } elseif ($data->kebutuhan_sehari_hari == 4) {
            $data['kebutuhan_m'] = 0.75;
        } elseif ($data->kebutuhan_sehari_hari == 5) {
            $data['kebutuhan_m'] = 1;
        }

        // kebutuhan u
        if ($data->kebutuhan_sehari_hari == 1) {
            $data['kebutuhan_u'] = 0.25;
        } elseif ($data->kebutuhan_sehari_hari == 2) {
            $data['kebutuhan_u'] = 0.5;
        } elseif ($data->kebutuhan_sehari_hari == 3) {
            $data['kebutuhan_u'] = 0.75;
        } elseif ($data->kebutuhan_sehari_hari == 4) {
            $data['kebutuhan_u'] = 1;
        } elseif ($data->kebutuhan_sehari_hari == 5) {
            $data['kebutuhan_u'] = 1;
        }

        // kondisi_rumah l
        if ($data->kondisi_rumah == 1) {
            $data['kondisi_rumah_l'] = 0;
        } elseif ($data->kondisi_rumah == 2) {
            $data['kondisi_rumah_l'] = 0;
        } elseif ($data->kondisi_rumah == 3) {
            $data['kondisi_rumah_l'] = 0.25;
        } elseif ($data->kondisi_rumah == 4) {
            $data['kondisi_rumah_l'] = 0.5;
        } elseif ($data->kondisi_rumah == 5) {
            $data['kondisi_rumah_l'] = 0.75;
        }
        // kondisi_rumah m
        if ($data->kondisi_rumah == 1) {
            $data['kondisi_rumah_m'] = 0;
        } elseif ($data->kondisi_rumah == 2) {
            $data['kondisi_rumah_m'] = 0.25;
        } elseif ($data->kondisi_rumah == 3) {
            $data['kondisi_rumah_m'] = 0.5;
        } elseif ($data->kondisi_rumah == 4) {
            $data['kondisi_rumah_m'] = 0.75;
        } elseif ($data->kondisi_rumah == 5) {
            $data['kondisi_rumah_m'] = 1;
        }

        // kondisi_rumah u
        if ($data->kondisi_rumah == 1) {
            $data['kondisi_rumah_u'] = 0.25;
        } elseif ($data->kondisi_rumah == 2) {
            $data['kondisi_rumah_u'] = 0.5;
        } elseif ($data->kondisi_rumah == 3) {
            $data['kondisi_rumah_u'] = 0.75;
        } elseif ($data->kondisi_rumah == 4) {
            $data['kondisi_rumah_u'] = 1;
        } elseif ($data->kondisi_rumah == 5) {
            $data['kondisi_rumah_u'] = 1;
        }

        // tanggungan l
        if ($data->tanggungan == 1) {
            $data['kondisi_rumah_l'] = 0;
        } elseif ($data->tanggungan == 2) {
            $data['kondisi_rumah_l'] = 0;
        } elseif ($data->tanggungan == 3) {
            $data['kondisi_rumah_l'] = 0.25;
        } elseif ($data->tanggungan == 4) {
            $data['kondisi_rumah_l'] = 0.5;
        } elseif ($data->tanggungan == 5) {
            $data['kondisi_rumah_l'] = 0.75;
        }
        // tanggungan m
        if ($data->tanggungan == 1) {
            $data['tanggungan_m'] = 0;
        } elseif ($data->tanggungan == 2) {
            $data['tanggungan_m'] = 0.25;
        } elseif ($data->tanggungan == 3) {
            $data['tanggungan_m'] = 0.5;
        } elseif ($data->tanggungan == 4) {
            $data['tanggungan_m'] = 0.75;
        } elseif ($data->tanggungan == 5) {
            $data['tanggungan_m'] = 1;
        }
        // tanggungan u
        if ($data->tanggungan == 1) {
            $data['tanggungan_u'] = 0.25;
        } elseif ($data->tanggungan == 2) {
            $data['tanggungan_u'] = 0.5;
        } elseif ($data->tanggungan == 3) {
            $data['tanggungan_u'] = 0.75;
        } elseif ($data->tanggungan == 4) {
            $data['tanggungan_u'] = 1;
        } elseif ($data->tanggungan == 5) {
            $data['tanggungan_u'] = 1;
        }

        // matrik usia
        $data['matrik_usia'] = ($data['usia_l'] + $data['usia_m'] + $data['usia_u']) / 3;
        // matrik pendapatan
        $data['matrik_pendapatan'] = ($data['pendapatan_l'] + $data['pendapatan_m'] + $data['pendapatan_u']) / 3;
        // matrik kebutuhan
        $data['matrik_kebutuhan'] = ($data['kebutuhan_l'] + $data['kebutuhan_m'] + $data['kebutuhan_u']) / 3;
        // matrik kondisi_rumah
        $data['matrik_kondisi_rumah'] = ($data['kondisi_rumah_l'] + $data['kondisi_rumah_m'] + $data['kondisi_rumah_u']) / 3;
        // matrik tanggungan
        $data['matrik_tanggungan'] = ($data['tanggungan_l'] + $data['tanggungan_m'] + $data['tanggungan_u']) / 3;
        // pangkat usia
        $data['pangkat_usia'] = pow($data['matrik_usia'], 2);
        // pangkat pendapatan
        $data['pangkat_pendapatan'] = pow($data['matrik_pendapatan'], 2);
        // pangkat kebutuhan
        $data['pangkat_kebutuhan'] = pow($data['matrik_kebutuhan'], 2);
        // pangkat kondisi_rumah
        $data['pangkat_kondisi_rumah'] = pow($data['matrik_kondisi_rumah'], 2);
        // pangkat tanggungan
        $data['pangkat_tanggungan'] = pow($data['matrik_tanggungan'], 2);
        // keputusan ternormalisasi usia
        $data['keputusan_ternormalisasi_usia'] = $data['matrik_usia'] / sqrt(array_sum([$data['pangkat_usia'], $data['pangkat_pendapatan'], $data['pangkat_kebutuhan'], $data['pangkat_kondisi_rumah'], $data['pangkat_tanggungan']]));
        // keputusan ternormalisasi pendapatan
        $data['keputusan_ternormalisasi_pendapatan'] = $data['matrik_pendapatan'] / sqrt(array_sum([$data['pangkat_usia'], $data['pangkat_pendapatan'], $data['pangkat_kebutuhan'], $data['pangkat_kondisi_rumah'], $data['pangkat_tanggungan']]));
        // keputusan ternormalisasi kebutuhan
        $data['keputusan_ternormalisasi_kebutuhan'] = $data['matrik_kebutuhan'] / sqrt(array_sum([$data['pangkat_usia'], $data['pangkat_pendapatan'], $data['pangkat_kebutuhan'], $data['pangkat_kondisi_rumah'], $data['pangkat_tanggungan']]));
        // keputusan ternormalisasi kondisi_rumah
        $data['keputusan_ternormalisasi_kondisi_rumah'] = $data['matrik_kondisi_rumah'] / sqrt(array_sum([$data['pangkat_usia'], $data['pangkat_pendapatan'], $data['pangkat_kebutuhan'], $data['pangkat_kondisi_rumah'], $data['pangkat_tanggungan']]));
        // keputusan ternormalisasi tanggungan
        $data['keputusan_ternormalisasi_tanggungan'] = $data['matrik_tanggungan'] / sqrt(array_sum([$data['pangkat_usia'], $data['pangkat_pendapatan'], $data['pangkat_kebutuhan'], $data['pangkat_kondisi_rumah'], $data['pangkat_tanggungan']]));
        // keputusan ternormalisasi usia bobot
        $data['keputusan_ternormalisasi_usia_bobot'] = $data['keputusan_ternormalisasi_usia'] * $bobot['0']->bobot;
        // keputusan ternormalisasi pendapatan bobot
        $data['keputusan_ternormalisasi_pendapatan_bobot'] = $data['keputusan_ternormalisasi_pendapatan'] * $bobot['1']->bobot;
        // keputusan ternormalisasi kebutuhan bobot
        $data['keputusan_ternormalisasi_kebutuhan_bobot'] = $data['keputusan_ternormalisasi_kebutuhan'] * $bobot['2']->bobot;
        // keputusan ternormalisasi kondisi_rumah bobot
        $data['keputusan_ternormalisasi_kondisi_rumah_bobot'] = $data['keputusan_ternormalisasi_kondisi_rumah'] * $bobot['3']->bobot;
        // keputusan ternormalisasi tanggungan bobot
        $data['keputusan_ternormalisasi_tanggungan_bobot'] = $data['keputusan_ternormalisasi_tanggungan'] * $bobot['4']->bobot;
        return $data;
    }
}
