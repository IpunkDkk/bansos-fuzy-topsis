<?php

namespace App\Http\Controllers;

use App\Models\Bobot;

class BobotController extends Controller
{
    public function index()
    {
        $kriteria = Bobot::all();
        return view('kriteria.index', compact('kriteria'));
    }
    public function edit($id)
    {
        $kriteria = Bobot::find($id);
        return view('kriteria.edit', compact('kriteria'));
    }
    public function update($id)
    {
        $kriteria = Bobot::find($id);
        $kriteria->update([
            'kode' => request('kode'),
            'nilai_kriteria' => request('nilai_kriteria'),
            'keterangan' => request('keterangan'),
            'bobot' => request('bobot'),
        ]);
        return redirect()->route('kriteria.index')->with('success', 'Data berhasil diubah');
    }
}
