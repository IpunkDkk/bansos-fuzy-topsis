<?php

namespace App\Http\Controllers;

use App\Models\Bobot;
use App\Models\Data;

class DashboardController extends Controller
{
    public function index()
    {
        $alldata = Data::all()->count();
        $allkriteria = Bobot::all()->count();
        return view('dashboard', compact('alldata', 'allkriteria'));
    }
}
